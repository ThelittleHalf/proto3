﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem; 


public class PlayerControles : MonoBehaviour, IBulletActivated
{
    public Color[] playerColors;

    public SpriteRenderer triangle;
    ShakeBehavior shak;
    public float move=0;
    public float lerpT=0.5f;
    public float speed=10;
    // pour le lerp
    public Vector2 botPos;
    public Vector2 topPos;

    public bool leftPlayer = true;

    Vector2 aimDir = Vector2.zero;
    Vector2 mousePos=Vector2.zero;
    Vector2 hitPos = Vector2.zero;
    Vector2 startPos = Vector2.zero;

    public LayerMask mask;
    float angle = 180;

    Manager man;
    int life = 5;
    public int id = 0;

    public float reloadTime = 1;
    bool canShoot = false;

    public float weaponTime = 3;

    public GameObject projectile;

    SpriteRenderer sr;

    LineRenderer lr;

    public SpriteRenderer[] lives;

    public ShotType[] weapons;
    public int currentWeapon = 0;

    public  float blinkTime = 0.1f; //<=temps pour créer le blink
    public float trsTime = 3f;// <= temps de "transparence" quand le joueur est touché
    public GameObject heart;
    public float maxtrsTime = 3;
    bool canBeHit = true;

    public Color[] weaponCol;

    public int altWeapon = 0;
    bool altActive = false;

    public Item currentItem=null;
    bool canSwitch = true;

    public List<LifeAnim> hearts;

    public bool active = false;

    private void Awake()
    {
        shak = FindObjectOfType<ShakeBehavior>();
        sr = GetComponent<SpriteRenderer>();
        man = FindObjectOfType<Manager>();

        lr = GetComponent<LineRenderer>();
        lr.enabled = false;
        StartCoroutine(Starting());
    }

    IEnumerator Starting()
    {
        yield return 0;
        triangle.color = Color.clear;
        for (int i = 0; i < hearts.Count; i++)
        {

            if (leftPlayer)
            {
                sr.flipX=true;
                sr.color = playerColors[0];
                lives[i].color = playerColors[0];
                hearts[i].lerpedColor = playerColors[0];
            }
            else
            {
                sr.color = playerColors[1];
                lives[i].color = playerColors[1];
                hearts[i].lerpedColor = playerColors[1];
            }
        }
        lr.enabled = true;
        if (leftPlayer)
        {
            startPos = (Vector2)transform.position + new Vector2(0.25f, 0);
            lr.startColor = Color.red;
            lr.endColor = Color.red;
        }
        else
        {
            startPos = (Vector2)transform.position + new Vector2(-0.25f, 0);
            lr.startColor = Color.blue;
            lr.endColor = Color.blue;
        }
    }

    private void FixedUpdate()
    {
        if (active)
        {
            lerpT += move * Time.deltaTime * speed;
            if (lerpT > 1)
            {
                lerpT = 1;
            }
            else if (lerpT < 0)
            {
                lerpT = 0;
            }

            transform.position = Vector2.Lerp(botPos, topPos, lerpT);
            if (leftPlayer)
            {
                startPos = (Vector2)transform.position + new Vector2(0.25f, 0);
            }
            else
            {
                startPos = (Vector2)transform.position + new Vector2(-0.25f, 0);
            }

            mousePos = transform.position + new Vector3(aimDir.x, aimDir.y, 0) * 1000;

            angle = Mathf.Atan2(mousePos.y, mousePos.x) * Mathf.Rad2Deg;

            if (lr.enabled)
            {
                RaycastHit2D hit = Physics2D.Raycast(transform.position, new Vector2(mousePos.x, mousePos.y), 1000, mask);
                if (hit)
                {
                    //Le point d'arrivée est placé là où le rayon s'arrête
                    hitPos = hit.point;
                }
                else
                {

                    hitPos = (Vector2)transform.position + (mousePos * 1000);
                }

                //On place entre le point de départ du renderer sur le point de tir, et son point d'arrivée sur la position du rayon, pour créer le laser visible
                lr.SetPosition(0, startPos);
                lr.SetPosition(1, hitPos);
            }



            if (reloadTime > 0)
            {
                reloadTime -= Time.deltaTime;


                if (reloadTime <= 0)
                {
                    canShoot = true;
                }
            }

            if (reloadTime <= 0 && canBeHit)
            {
            }

            if (altWeapon != 0)
            {
                if (altActive)
                {
                    triangle.color = Color.white;
                }
                else
                {
                    triangle.color = weaponCol[altWeapon];
                }

            }
            else
            {
                triangle.color = Color.clear;
            }
        }
        
    }



    public void OnVertical(InputValue value)
    {
        if (active)
        {
            float temp = value.Get<float>();

            if (temp > 0.3f || temp < -0.3f)
            {
                move = temp;
            }
            else
            {
                move = 0;
            }
        }
        

        
    }

    public void OnVisee(InputValue value)
    {
        Vector2 temp = value.Get<Vector2>();
            aimDir.y = temp.y;

        if (temp.x > 0.1f || temp.x < -0.1f)
        {

            if (leftPlayer)
            {
                    aimDir.x = Mathf.Abs(temp.x);
            }
            else
            {
                aimDir.x = -Mathf.Abs(temp.x);
            }
        }

        
    }

    public void OnShoot()
    {
        if (canShoot && active)
        {
            if (altActive)
            {
                currentWeapon = altWeapon;

            }
            else
            {
                currentWeapon = 0;
            }
            StartCoroutine(ShootingCo());
            
        }
        
    }

    public void OnWeaponSwitch()
    {
        if (canSwitch)
        {
            if (altWeapon != 0)
            {
                altActive = !altActive;

                StartCoroutine(WaitSwitch());
            }

            if (altActive)
            {
                currentWeapon = altWeapon;
            }
            else
            {
                currentWeapon = 0;
            }
        }
        

        
    }

    public void Touched(int id, int dam)
    {
        if (canBeHit && id!=this.id)
        {
            life -= dam;
            shak.TriggerShake(0.5f * dam, 0.3f * dam, 1);
            if (life <= 0)
            {
                man.Win(id);
                gameObject.SetActive(false);
            }
            else
            {
                StartCoroutine(Fading());
                for (int i = hearts.Count-1; i>-1; i--)
                {
                    if (i >= life)
                    {
                        hearts[i].LoseLife();
                        hearts.RemoveAt(i);
                    }
                }
            }
            
        }
    }


    IEnumerator ShootingCo()
    {
        ShotType st = weapons[currentWeapon];
        canShoot = false;
        reloadTime = st.reloadTime;
        for (int i = 0; i < st.nShots; i++)
        {
            if (st.spread)
            {
                for (int j = 0; j < st.nShotsSpread; j++)
                {
                    float newAngle = (angle - st.maxAngle) + (((st.maxAngle * 2) / (st.nShotsSpread-1)) * j);
                    Debug.Log(j + " angle =" + newAngle);
                    Vector2 newAim = Vector2.zero;

                    newAim.x = Mathf.Cos(newAngle * Mathf.Deg2Rad);
                    newAim.y = Mathf.Sin(newAngle * Mathf.Deg2Rad);

                    GameObject tempS = Instantiate(projectile, transform.position, Quaternion.identity);
                    if (leftPlayer)
                    {
                        tempS.GetComponent<TrailRenderer>().startColor = playerColors[0];
                        tempS.GetComponent<TrailRenderer>().endColor = new Color(playerColors[0].r, playerColors[0].g, playerColors[0].b,0.5f);
                    }
                    else
                    {
                        tempS.GetComponent<TrailRenderer>().startColor = playerColors[1];
                        tempS.GetComponent<TrailRenderer>().endColor = new Color(playerColors[1].r, playerColors[1].g, playerColors[1].b, 0.5f);
                    }
                    
                    TesterTir ttS = tempS.GetComponent<TesterTir>();
                    ttS.speed = st.speed;
                    ttS.damage = st.damage;
                    ttS.lifetime = st.lifetime;
                    tempS.transform.localScale = st.shotSize;
                    ttS.id = id;
                    ttS.StartForce(newAim.normalized);
                    if (leftPlayer)
                    {
                        tempS.GetComponent<SpriteRenderer>().color = playerColors[0];
                    }
                    else
                    {
                        tempS.GetComponent<SpriteRenderer>().color = playerColors[1];
                    }
                }
            }
            else
            {

                GameObject temp = Instantiate(projectile, transform.position, Quaternion.identity);
                if (leftPlayer)
                {
                    temp.GetComponent<TrailRenderer>().startColor = playerColors[0];
                    temp.GetComponent<TrailRenderer>().endColor = new Color(playerColors[0].r, playerColors[0].g, playerColors[0].b, 0.5f);
                }
                else
                {
                    temp.GetComponent<TrailRenderer>().startColor = playerColors[1];
                    temp.GetComponent<TrailRenderer>().endColor = new Color(playerColors[1].r, playerColors[1].g, playerColors[1].b, 0.5f);
                }
                TesterTir tt = temp.GetComponent<TesterTir>();
                tt.speed = st.speed;
                tt.damage = st.damage;
                tt.lifetime = st.lifetime;
                temp.transform.localScale = st.shotSize;
                tt.id = id;
                tt.StartForce(mousePos.normalized);
                if (leftPlayer)
                {
                    temp.GetComponent<SpriteRenderer>().color = playerColors[0];
                }
                else
                {
                    temp.GetComponent<SpriteRenderer>().color = playerColors[1];
                }
            }

            

            yield return new WaitForSeconds(st.timeBetweenShots);
        }
    }

    IEnumerator Fading()
    {
        trsTime = maxtrsTime;
        canBeHit = false;
        while (trsTime >= 0)
        {
            sr.color = new Color(sr.color.r, sr.color.g, sr.color.b, .5f);
            trsTime -= blinkTime;
            yield return new WaitForSeconds(blinkTime);
            sr.color = new Color(sr.color.r, sr.color.g, sr.color.b, 1f);
            trsTime -= blinkTime;
            yield return new WaitForSeconds(blinkTime);
        }
        sr.color = new Color(sr.color.r, sr.color.g, sr.color.b, 1f);
        canBeHit = true;
    }

    IEnumerator WaitSwitch()
    {
        canSwitch = false;
        yield return new WaitForSeconds(0.5f);
        canSwitch = true;
    }

}
