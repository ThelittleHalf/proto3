﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuScript : MonoBehaviour
{
    int choice=0;
    public Vector2[] positions;
    public Image curseur;
    public int stageNumber;
    bool canMove = true;

    private void Update()
    {
        curseur.transform.localPosition = positions[choice];
    }

    public void OnValidation()
    {
        if (choice < stageNumber)
        {
            SceneManager.LoadScene("Tableau" + choice);
        }
        else if (choice == stageNumber)
        {
            Application.Quit();
        }
    }

    public void OnVertical(InputValue value)
    {
        float temp = value.Get<float>();

        if (canMove)
        {
            if (temp > 0.3f)
            {
                if (choice > 0)
                {
                    choice--;
                }
                else
                {
                    choice = stageNumber;
                }

                StartCoroutine(MoveAgain());
            }
            else if (temp <= -0.3f)
            {
                if (choice <stageNumber)
                {
                    choice++;
                }
                else
                {
                    choice =0;
                }
                StartCoroutine(MoveAgain());
            }
        }
       

    }

    IEnumerator MoveAgain()
    {
        canMove = false;
        yield return new WaitForSeconds(0.25f);
        canMove = true;
    }
}
