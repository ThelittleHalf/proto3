﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemBox : MonoBehaviour, IBulletActivated
{
    public Item[] items;
    PlayerControles[] players;
    public Gradient grad;
    SpriteRenderer sr;
    float colorTime = 0;
    BoxCollider2D box;

    private void Awake()
    {
        players= FindObjectsOfType<PlayerControles>();
        sr = GetComponent<SpriteRenderer>();
        box = GetComponent<BoxCollider2D>();
        StartCoroutine(WaitBox());
    }

    private void Update()
    {
        colorTime += Time.deltaTime;
        if (colorTime > 1)
        {
            colorTime = 0;
        }
        sr.color = grad.Evaluate(colorTime);
    }

    public void Touched(int id, int dam)
    {
        for (int i = 0; i < players.Length; i++)
        {
            if (players[i].id == id)
            {
                int rand = Random.Range(0, items.Length);
                if (items[rand].weaponId > 0)
                {
                    players[i].altWeapon = items[rand].weaponId;
                }
                else
                {
                    players[i].currentItem = items[rand];
                }

            }
        }

        Destroy(gameObject);
    }

    IEnumerator WaitBox()
    {
        box.enabled = false;
        yield return new WaitForSeconds(1.5f);
        box.enabled = true;
    }
}
