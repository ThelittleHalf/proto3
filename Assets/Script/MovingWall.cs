﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingWall : MonoBehaviour
{
    public Vector2 startPos, endPos;
    public float lerpTime = 0.5f;
    public float lerpSpeed = 1;
    bool goingUp = true;

    private void Update()
    {
        if (goingUp)
        {
            lerpTime += Time.deltaTime * lerpSpeed;
            if (lerpTime >= 1)
            {
                lerpTime = 1;
                goingUp = false;
            }
        }
        else
        {
            lerpTime -= Time.deltaTime * lerpSpeed;
            if (lerpTime <= 0)
            {
                lerpTime = 0;
                goingUp = true;
            }
        }
        transform.position = Vector2.Lerp(startPos, endPos, lerpTime);
    }
}
