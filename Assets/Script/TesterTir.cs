﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TesterTir : MonoBehaviour
{
    public float speed;
    public int id=-1;
    public int damage = 1;
    Rigidbody2D rb;
    public float lifetime = 10;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }
    private void Update()
    {
        lifetime -= Time.deltaTime;
        if (lifetime <= 0)
        {
            Destroy(gameObject);
        }
    }

    public void StartForce(Vector2 str)
    {
        rb.AddForce(str * speed);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.GetComponent<IBulletActivated>() != null)
        {
            collision.GetComponent<IBulletActivated>().Touched(id, damage);
            if (collision.GetComponent<PlayerControles>() != null)
            {
                if (collision.GetComponent<PlayerControles>().id != id)
                {
                    Destroy(gameObject);
                }
            }
            else
            {
                Destroy(gameObject);
            }
            
        }
    }
}
