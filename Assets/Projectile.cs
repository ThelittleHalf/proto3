﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Projectile : MonoBehaviour
{
    Rigidbody2D rb;
    //Vector2 direction; <= pas nécessaire
    Vector2 lastVelocity;
    public float speed;



    // Start is called before the first frame update
    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();

    }


    // Update is called once per frame
    void Update()
    {
        //direction = new Vector2(gameObject.transform.position.x, gameObject.transform.position.y);
        lastVelocity = rb.velocity;
        //rb.position += Vector2.right * 0.1f;

    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Destroy(gameObject);
        } 
        else if (collision.gameObject.tag == "Obstacle")
        {
            speed = lastVelocity.magnitude;
            lastVelocity = Vector2.Reflect(rb.velocity.normalized, collision.contacts[0].normal) ;
            /*rb.velocity = rb.velocity * lastVelocity.normalized;
            transform.rotation = Quaternion.identity;
            transform.Rotate(new Vector3(0,0, -90+Mathf.Atan2(transform.position.y, transform.position.x) * Mathf.Rad2Deg));*/
            rb.velocity = lastVelocity * Mathf.Max(0f,speed);
        }


    }

}
