﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "newShot", menuName = "ShotType", order = 1)]
public class ShotType : ScriptableObject
{
    public bool spread = false;
    public float reloadTime;
    public int nShots = 1;
    public Vector3 shotSize = new Vector3(0.1f, .1f, 1);
    public int damage=1;
    public float lifetime=4;
    public float speed = 50;

    public float maxAngle=0;
    public int nShotsSpread = 0;
    public float timeBetweenShots=0.25f;
}
