﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SummonItem : MonoBehaviour
{
    public ItemBox box;

    public Vector2[] positions;
    public bool[] summoned;

    public float maxTimer, timer;

    private void Update()
    {
        timer -= Time.deltaTime;
        if (timer <= 0)
        {
            timer = maxTimer;

            int rand = Random.Range(0, positions.Length);
            bool ok = false;

            for (int i = 0; i < positions.Length; i++)
            {
                if (!summoned[i])
                {
                    ok = true;
                }
            }

            if (ok)
            {
                while (summoned[rand])
                {
                    rand= Random.Range(0, positions.Length);
                }
            }

            Instantiate(box, positions[rand], Quaternion.identity);
        }
    }
}
