﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatingObject : MonoBehaviour
{
    public float speed;
    void Update()
    {
        gameObject.transform.Rotate(0, 0, speed);
    }
}
