﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IBulletActivated
{
    void Touched(int id, int dam);
}
