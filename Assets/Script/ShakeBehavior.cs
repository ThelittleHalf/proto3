﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShakeBehavior : MonoBehaviour
{

    private Transform trans;
    public float shakeDuration = 0f; //<== durée du shake
    public float shakeMagnitude = 0.7f; // <== sa magnitude
    private float dampingSpeed = 1.0f; // à quelle vitesse ça doit disparaître
    Vector3 initialPosition;

    void Awake()
    {
        // Garder le transform du game Object
        if (transform == null)
        {
            trans = GetComponent(typeof(Transform)) as Transform;
        }
    }

    // garder la position initiale de la caméra
    void OnEnable()
    {
        initialPosition = transform.localPosition;
    }

    // si on veut modifier le shake via d'autres scripts ou autre et rapidos
    public void TriggerShake(float duration, float magnitude, float damping)
    {
        //Utiliser des variables dans la fonction pour pouvoir faire des shake différents selon la situation.
        shakeDuration = duration;
        shakeMagnitude = magnitude;
        dampingSpeed = damping;
    }

    // le shake
    void Update()
    {
        if (shakeDuration > 0)
        {
            transform.localPosition = initialPosition + Random.insideUnitSphere * shakeMagnitude; // insideUnitySphere, c'est un point random dans une sphèr avec un radius 1

            //le temps que ça disparaisse
            shakeDuration -= Time.deltaTime * dampingSpeed;
        }
        else
        {
            shakeDuration = 0f;
            transform.localPosition = initialPosition;
        }


    }
}
