﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeAnim : MonoBehaviour
{
    public GameObject life;
    public GameObject heart;
    
    float speed = 0.1f;
    float temps = 0.2f;
    float t = 0f;
    //[SerializeField] [Range(0f, 1f)] float lerpTime;
    public Color lerpedColor = Color.red;
    Vector3 initialSize;
    public SpriteRenderer render;
    // Start is called before the first frame update
    void Awake()
    {

        life.SetActive(true);
        heart.SetActive(false);
        initialSize = transform.localScale;
        
        render = heart.GetComponent<SpriteRenderer>();
        //render.material.color = lerpedColor;

    }

    public void LoseLife()
    {
        StartCoroutine(DestroyingLife());
    }

    public IEnumerator DestroyingLife()
    {
        life.SetActive(false);
        heart.SetActive(true);
        while (t < 0.8)
        {

            t += Time.deltaTime * 2;
            render.material.color = Color.Lerp(lerpedColor, Color.white, t);
            heart.transform.localScale += new Vector3(1, 1, 1) * Time.deltaTime * speed * 5f;
            yield return 0;
        }
        while (t>=0.8 && t<1)
        {
            t += Time.deltaTime ;
            render.material.color = Color.Lerp(Color.white, Color.clear, t);
            heart.transform.localScale += new Vector3(1, 1, 1) * Time.deltaTime * speed *0.1f;

            yield return 0;
        }

        yield return new WaitForSeconds(0.5f);
        Destroy(gameObject);
    }
}
