// GENERATED AUTOMATICALLY FROM 'Assets/Script/InputPlayers.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @InputPlayers : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @InputPlayers()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""InputPlayers"",
    ""maps"": [
        {
            ""name"": ""Mouvements"",
            ""id"": ""085128b6-2c5f-420f-9e3d-bd8952435459"",
            ""actions"": [
                {
                    ""name"": ""Vertical"",
                    ""type"": ""Value"",
                    ""id"": ""bf53e9cf-b23d-4f49-8a7d-daecfbfffedb"",
                    ""expectedControlType"": ""Axis"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Visee"",
                    ""type"": ""Value"",
                    ""id"": ""ea8e05b1-c35e-46a5-b83c-2d718a43992a"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Shoot"",
                    ""type"": ""Button"",
                    ""id"": ""b0b3452c-956d-48ab-857b-29015830dbcd"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""WeaponSwitch"",
                    ""type"": ""Button"",
                    ""id"": ""ff1828b8-f706-4ffe-9633-dae268a551fb"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Validation"",
                    ""type"": ""Button"",
                    ""id"": ""7dc5c756-8242-467d-a22a-163d6a39a26a"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""048f9b5f-2a50-435c-96ff-7b5a6dbfce4f"",
                    ""path"": ""<Gamepad>/leftStick/y"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PlayerInputs"",
                    ""action"": ""Vertical"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""55c633c5-53fe-42f2-96f4-26d42d5c3322"",
                    ""path"": ""<Gamepad>/rightStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PlayerInputs"",
                    ""action"": ""Visee"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""2db439a0-9897-4f2f-84a3-b84612144763"",
                    ""path"": ""<Gamepad>/rightShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Shoot"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""82ef1e09-aa20-45fe-9365-eec371a55dff"",
                    ""path"": ""<Gamepad>/leftShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Shoot"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""1e959673-81b0-445a-b726-5c9604157640"",
                    ""path"": ""<Gamepad>/leftTrigger"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""WeaponSwitch"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""a6e1a897-0bae-42e2-813d-3d69f5d8c005"",
                    ""path"": ""<Gamepad>/rightTrigger"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""WeaponSwitch"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""c8b18585-e7f3-4296-ad15-2ddcc5f43f77"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Validation"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": [
        {
            ""name"": ""PlayerInputs"",
            ""bindingGroup"": ""PlayerInputs"",
            ""devices"": [
                {
                    ""devicePath"": ""<Gamepad>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        }
    ]
}");
        // Mouvements
        m_Mouvements = asset.FindActionMap("Mouvements", throwIfNotFound: true);
        m_Mouvements_Vertical = m_Mouvements.FindAction("Vertical", throwIfNotFound: true);
        m_Mouvements_Visee = m_Mouvements.FindAction("Visee", throwIfNotFound: true);
        m_Mouvements_Shoot = m_Mouvements.FindAction("Shoot", throwIfNotFound: true);
        m_Mouvements_WeaponSwitch = m_Mouvements.FindAction("WeaponSwitch", throwIfNotFound: true);
        m_Mouvements_Validation = m_Mouvements.FindAction("Validation", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Mouvements
    private readonly InputActionMap m_Mouvements;
    private IMouvementsActions m_MouvementsActionsCallbackInterface;
    private readonly InputAction m_Mouvements_Vertical;
    private readonly InputAction m_Mouvements_Visee;
    private readonly InputAction m_Mouvements_Shoot;
    private readonly InputAction m_Mouvements_WeaponSwitch;
    private readonly InputAction m_Mouvements_Validation;
    public struct MouvementsActions
    {
        private @InputPlayers m_Wrapper;
        public MouvementsActions(@InputPlayers wrapper) { m_Wrapper = wrapper; }
        public InputAction @Vertical => m_Wrapper.m_Mouvements_Vertical;
        public InputAction @Visee => m_Wrapper.m_Mouvements_Visee;
        public InputAction @Shoot => m_Wrapper.m_Mouvements_Shoot;
        public InputAction @WeaponSwitch => m_Wrapper.m_Mouvements_WeaponSwitch;
        public InputAction @Validation => m_Wrapper.m_Mouvements_Validation;
        public InputActionMap Get() { return m_Wrapper.m_Mouvements; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(MouvementsActions set) { return set.Get(); }
        public void SetCallbacks(IMouvementsActions instance)
        {
            if (m_Wrapper.m_MouvementsActionsCallbackInterface != null)
            {
                @Vertical.started -= m_Wrapper.m_MouvementsActionsCallbackInterface.OnVertical;
                @Vertical.performed -= m_Wrapper.m_MouvementsActionsCallbackInterface.OnVertical;
                @Vertical.canceled -= m_Wrapper.m_MouvementsActionsCallbackInterface.OnVertical;
                @Visee.started -= m_Wrapper.m_MouvementsActionsCallbackInterface.OnVisee;
                @Visee.performed -= m_Wrapper.m_MouvementsActionsCallbackInterface.OnVisee;
                @Visee.canceled -= m_Wrapper.m_MouvementsActionsCallbackInterface.OnVisee;
                @Shoot.started -= m_Wrapper.m_MouvementsActionsCallbackInterface.OnShoot;
                @Shoot.performed -= m_Wrapper.m_MouvementsActionsCallbackInterface.OnShoot;
                @Shoot.canceled -= m_Wrapper.m_MouvementsActionsCallbackInterface.OnShoot;
                @WeaponSwitch.started -= m_Wrapper.m_MouvementsActionsCallbackInterface.OnWeaponSwitch;
                @WeaponSwitch.performed -= m_Wrapper.m_MouvementsActionsCallbackInterface.OnWeaponSwitch;
                @WeaponSwitch.canceled -= m_Wrapper.m_MouvementsActionsCallbackInterface.OnWeaponSwitch;
                @Validation.started -= m_Wrapper.m_MouvementsActionsCallbackInterface.OnValidation;
                @Validation.performed -= m_Wrapper.m_MouvementsActionsCallbackInterface.OnValidation;
                @Validation.canceled -= m_Wrapper.m_MouvementsActionsCallbackInterface.OnValidation;
            }
            m_Wrapper.m_MouvementsActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Vertical.started += instance.OnVertical;
                @Vertical.performed += instance.OnVertical;
                @Vertical.canceled += instance.OnVertical;
                @Visee.started += instance.OnVisee;
                @Visee.performed += instance.OnVisee;
                @Visee.canceled += instance.OnVisee;
                @Shoot.started += instance.OnShoot;
                @Shoot.performed += instance.OnShoot;
                @Shoot.canceled += instance.OnShoot;
                @WeaponSwitch.started += instance.OnWeaponSwitch;
                @WeaponSwitch.performed += instance.OnWeaponSwitch;
                @WeaponSwitch.canceled += instance.OnWeaponSwitch;
                @Validation.started += instance.OnValidation;
                @Validation.performed += instance.OnValidation;
                @Validation.canceled += instance.OnValidation;
            }
        }
    }
    public MouvementsActions @Mouvements => new MouvementsActions(this);
    private int m_PlayerInputsSchemeIndex = -1;
    public InputControlScheme PlayerInputsScheme
    {
        get
        {
            if (m_PlayerInputsSchemeIndex == -1) m_PlayerInputsSchemeIndex = asset.FindControlSchemeIndex("PlayerInputs");
            return asset.controlSchemes[m_PlayerInputsSchemeIndex];
        }
    }
    public interface IMouvementsActions
    {
        void OnVertical(InputAction.CallbackContext context);
        void OnVisee(InputAction.CallbackContext context);
        void OnShoot(InputAction.CallbackContext context);
        void OnWeaponSwitch(InputAction.CallbackContext context);
        void OnValidation(InputAction.CallbackContext context);
    }
}
