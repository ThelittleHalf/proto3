﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Manager : MonoBehaviour
{
    public GameObject player;
    public Text winner;
    public List<PlayerControles> playersInGame;
    public Vector2[] playerPos;

    public GameObject controlInfos;
    public Text timerText;

    private void Start()
    {
        StartCoroutine(SummonPlayers());
        winner.gameObject.SetActive(false);
    }


    IEnumerator SummonPlayers()
    {
        controlInfos.SetActive(true);
        timerText.text = "3";
        timerText.color = Color.green;
        GameObject temp = Instantiate(player, transform.position, Quaternion.identity);
        temp.transform.position=new Vector3(playerPos[0].x, (playerPos[0].y+ playerPos[1].y)/2, -5);
        PlayerControles pc = temp.GetComponent<PlayerControles>();
        pc.botPos = playerPos[0];
        pc.topPos = playerPos[1];
        pc.leftPlayer = true;
        pc.id = 0;
        playersInGame.Add(pc);
        yield return 0;
        yield return 0;
        GameObject temp2 = Instantiate(player, transform.position, Quaternion.identity);
        temp2.transform.position = new Vector3(playerPos[2].x, (playerPos[2].y + playerPos[3].y) / 2, -5);
        PlayerControles pc2 = temp2.GetComponent<PlayerControles>();
        pc2.botPos = playerPos[2];
        pc2.topPos = playerPos[3];
        pc2.leftPlayer = false;
        pc2.id = 1;
        playersInGame.Add(pc2);

        yield return new WaitForSeconds(1);
        timerText.text = "2";
        timerText.color = Color.yellow;
        yield return new WaitForSeconds(1);
        timerText.text = "1";
        timerText.color = Color.red;
        yield return new WaitForSeconds(1);
        pc.active = true;
        pc2.active = true;

        controlInfos.SetActive(false);
    }

    public void Win(int loser)
    {
        winner.gameObject.SetActive(true);
        StartCoroutine(Again());
        if (loser == 0)
        {
            winner.text = "Player 2 wins";
        }
        else
        {
            winner.text = "Player 1 wins";
        }
    }

    IEnumerator Again()
    {
        yield return new WaitForSeconds(3);
        SceneManager.LoadScene("Menu");
    }
}
